<?php
/**
 * Market plugin for Craft CMS 3.x
 *
 * mojo Market a simple commerce plugin for Craft 3
 *
 * @link      https://www.mojointeractive.ch/
 * @copyright Copyright (c) 2018 mojo interactive GmbH
 */

/**
 * Market en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('market', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    mojo interactive GmbH
 * @package   Market
 * @since     1.0.0
 */
return [
    'Market plugin loaded' => 'Market plugin loaded',
];
