<?php
/**
 * Market plugin for Craft CMS 3.x
 *
 * mojo Market a simple commerce plugin for Craft 3
 *
 * @link      https://www.mojointeractive.ch/
 * @copyright Copyright (c) 2018 mojo interactive GmbH
 */

namespace mojointeractive\market\controllers;

use mojointeractive\market\Market;

use Craft;
use craft\web\Controller;

/**
 * Default Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your plugin’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    mojo interactive GmbH
 * @package   Market
 * @since     1.0.0
 */
class MarketController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = false;


    /**
     * Handle a request going to our plugin's actionDoSomething URL,
     * e.g.: actions/market/default/do-something
     *
     * @return mixed
     */
    public function actionGetCart()
    {
        return Market::$plugin->getCart();
    }

    /**
     * Fetches or creates an Entry.
     *
     * @return Poll
     * @throws NotFoundHttpException if the requested entry cannot be found
     */
    private function _getCart(bool $createCart = false): Entry
    {

        return Market::$plugin->MarketSercice->getCart($createCart);

    }

    /**
     * Fetches or creates an Entry.
     *
     * @return Poll
     * @throws NotFoundHttpException if the requested entry cannot be found
     */
    private function _checkItemInCart($cart, $product, $variation, $options) {

		// Get the sku
		$items = $cart->marketOrderItems;
		$response = false;

		foreach ($items as $item) {
			$variationMatch = false;
			if ($variation != false and $variation->id == $item->variationId) {
				$variationMatch = true;
			} elseif ($variation == false) {
				$variationMatch = true;
			}
			if($product->id == $item->product->first()->id and $variationMatch) {
				// Check if the same  Options are selected
				if (count($options) == count($item->options)) {
					if (count($options) == 0) {
						$response = $item->id;
						break;
					} else {
						$cartOptions = array();
						// $newOptions = array();
						// foreach ($options as $value) {
						// 	$newOptions[] = $value;
						// }
						foreach ($item->options as $obj) {
							$cartOptions[] = $obj['col4'];
						}
						if ($cartOptions === array_intersect($cartOptions, $options) && $options === array_intersect($options, $cartOptions)) {
							$response = $item->id;
							break;
						} else {
							$response = false;
						}
						// $response = $item->id;
					}
				} else {
					$response = false;
				}
			} else {
				$response = false;
			}
		}

		return $response;

	}

  private function _updateQuantity($cart, $itemId, $quantity) {

		$ownerId = $cart->id;
		$field = craft()->fields->getFieldByHandle("marketOrderItems");
		$blockTypes = craft()->matrix->getBlockTypesByFieldId($field->id);

		$block = craft()->matrix->getBlockById($itemId);
		$block->fieldId    = $field->id; // SuperTable field's ID
		$block->ownerId    = $ownerId; // ID of entry the block should be added to
		$block->typeId		 = $blockTypes[0]['id'];

		$block->getContent()->setAttributes(array(
			'quantity' => $block->quantity + $quantity
		));

		$success = craft()->matrix->saveBlock($block);
		if (!$success)
		{
				Craft::log('Couldn’t save the entry', LogLevel::Error);
				return $block->getErrors();
		} else {
			$cart->setContent(array(
				'marketOrderUpdated'										=> date("Y-m-d H:i:s")
			));

			// Save the updated Shipping infos
			$updateDate = craft()->entries->saveEntry($cart);
			if (!$updateDate)
			{
				Craft::log('Couldn’t save the entry', LogLevel::Error);
				return $block->getErrors();
			} else {
				return $success;
			}
		}

	}

  private function _addItemToCart($cart, $product, $variation, $options, $quantity) {

		$ownerId = $cart->id; // the parent entry
		$field = craft()->fields->getFieldByHandle("marketOrderItems");
		$blockTypes = craft()->matrix->getBlockTypesByFieldId($field->id);

		$block = new MatrixBlockModel();
		$block->fieldId    = $field->id; // SuperTable field's ID
		$block->ownerId    = $ownerId; // ID of entry the block should be added to
		$block->typeId		 = $blockTypes[0]['id'];

		// if ($variation != false && $variation->heading != '') {
		// 	$variationHeading = $variation->heading;
		// } else {
		// 	$variationHeading = '';
		// }
		//
		// if ($variation != false && $variation->sku != '') {
		// 	$sku = $variation->sku;
		// } else {
		// 	$sku = $product->marketSku;
		// }
		//
		if ($variation != false && $variation->price != '') {
			$price = $variation->price;
		} else {
			$price = $product->marketProductPrice;
		}
		//
		// if ($variation != false) {
		// 	$variationId = $variation->id;
		// } else {
		// 	$variationId = '';
		// }

		$addOptions = array();
		if (count($options) > 0) {
			foreach ($options as $value) {
				$productOption = craft()->superTable->getBlockById(($value));
				$opt = array(
					'col4' => $productOption->id,
					'col1' => $productOption->sku,
					'col2' => $productOption->heading,
					'col3' => $productOption->priceDifference
				);
				$addOptions[] = $opt;
			}
		} else {
			$addOptions = '';
		}

		if($variation != false) {
			$block->getContent()->setAttributes(array(
				'sku' => $product->marketProductSku,
				'description' => $product->marketProductName,
				'product' => array($product->id),
				'variationId' => $variation->id,
				'variationSku' => $variation->sku,
				'variationHeading' => $variation->heading,
				'quantity' => $quantity,
				'price' => $price,
				'options' => $addOptions,
			));
		} else {
			$block->getContent()->setAttributes(array(
				'sku' => $product->marketProductSku,
				'description' => $product->marketProductName,
				'product' => array($product->id),
				'variationId' => '',
				'variationSku' => '',
				'variationHeading' => '',
				'quantity' => $quantity,
				'price' => $price,
				'options' => $addOptions,
			));
		}

		$success = craft()->matrix->saveBlock($block);
		if (!$success)
		{
				Craft::log('Couldn’t save the entry', LogLevel::Error);
				return $block->getErrors();
		}
		else {
				$cart->setContent(array(
					'marketOrderUpdated'										=> date("Y-m-d H:i:s")
				));

			// Save the updated Shipping infos
			$updateDate = craft()->entries->saveEntry($cart);
		}

	}
}
