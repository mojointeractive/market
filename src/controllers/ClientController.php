<?php
/**
 * Polls plugin for Craft CMS 3.x
 *
 * A cool polls plugin for Craft CMS.
 *
 * @link      https://www.mojointeractive.ch/
 * @copyright Copyright (c) 2018 mojo interactive GmbH
 */

namespace mojointeractive\market\controllers;

use mojointeractive\market\Market;

use Craft;
use craft\web\Controller;
use craft\elements\Entry;

/**
 * Default Controller
 *
 * @author    mojo interactive GmbH
 * @package   Polls
 * @since     1.0.0
 */
class ClientController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = true;

    // Public Methods
    // =========================================================================

    /**
     * Handle a request going to our plugin's index action URL,
     * e.g.: actions/market/default
     *
     * @return mixed
     */
    public function actionAddItem()
    {
      $this->requirePostRequest();
      $request = Craft::$app->getRequest();
      
      // Check if we have a cart already and create one if needed
      $cart = $this->_getCart(true);
  
      // Try to get the Product
      $productId = $request->getParam('productId', '');
      $product = Market::$plugin->MarketService->getProductById($productId);
      
      // Get the selectet Variation of the Product (if there is one selected)
      
      if (!$request->getBodyParam('variationId')) {
        $variation = false;
      } else {
        $variation = Craft::$app->getMatrix()->getBlockById($request->getBodyParam('variationId'));
      }
      
      $quantity = $request->getBodyParam('quantity');
      $options = null;
      
      // Check if the Item is already in the cart and update the quantity if needed
      $itemInCartId = Market::$plugin->MarketService->checkItemInCart($cart, $productId, $variation, $options);
      if ($itemInCartId != false) {
        // Update the quantity
        Market::$plugin->MarketService->updateQuantity($cart, $itemInCartId, $quantity);
        // Return SUCCESS
      
        $response = array(
          'success' => true,
          'action' => 'Updated Quantity',
          'response' => $itemInCartId
        );
      } else {
        Market::$plugin->MarketService->addItemToCart($cart, $productId, $variation, $options, $quantity);
      
        $response = array(
          'success' => true,
          'action' => 'Added item to Cart',
          'response' => 'okidoki'
        );
      }

      if ($request->getAcceptsJson()) {
        return $this->asJson($response);
      }

    }

    public function actionUpdateQuantity()
    { 

      $this->requirePostRequest();
      $request = Craft::$app->getRequest();

      $itemId = $request->getParam('itemId', '');
      $action = $request->getParam('action', '');
  
      $cart = $this->_getCart(false);
  
      if ($cart) {
        $block = \craft\elements\MatrixBlock::find()
        ->id($itemId)
        ->one();

        if($action == '+') {
          $quantity = $block->quantity + 1;
          Market::$plugin->MarketService->updateQuantity($cart, $itemId, 1);
        } elseif ($action == '-') {
          $quantity = $block->quantity - 1;
          Market::$plugin->MarketService->updateQuantity($cart, $itemId, -1);
        }
      }
  
      $cartPrice = 0;
      $cartQuantity = 0;
      foreach ($cart->orderItems as $item) {
        $itemPrice = $item->price;
        $cartPrice = $cartPrice + $itemPrice * $item->quantity;
        $cartQuantity = $cartQuantity + $item->quantity;
      }
  
      $response = array(
        'success' => true,
        'message' => $itemId,
        'itemQuantity' => $quantity,
        'cartQuantity' => $cartQuantity,
        'cartPrice' => $cartPrice,
        'action' => 'success'
      );
  
      if ($request->getAcceptsJson()) {
        return $this->asJson($response);
      }
    }

    public function actionConfirmOrder() {



      $this->requirePostRequest();
      $request = Craft::$app->getRequest();
  
      $cart = $this->_getCart(false);

      if ($cart != null) {
        // We have a valid order

        $cart->title = 'Order #'.$cart->id;
        $cart->orderStatus = 'order';
        $cart->orderSecret = md5($cart->id.$cart->postDate->format('Y-m-d H:i:s'));
        $cart->orderDate =  date("Y-m-d H:i:s");

        $fields = $request->getParam('fields', '');
        
        $cart->setFieldValues($fields);

        // Save the updated Shipping infos
        if (!Craft::$app->getElements()->saveElement($cart)) {
          Craft::$app->getSession()->setError(Craft::t('market', 'Couldn’t save Order.'));
        } else {

          $html = "Vielen Dank. <br><br> Wir bearbeiten Ihre Bestellung so bald wie möglich. <br><br> Sie können Ihre Bestellung unter folgendem Link einsehen: <br><br> https://www.grappa-bruno.ch/market/order/" . $cart->id . "/" . $cart->orderSecret;
          $subject = 'grappe-bruno.ch - Bestellung';
          $mail = $cart->orderBillingEmail ;
          $attachments = array();
          Market::$plugin->MarketService->sendMail($html, $subject, $mail, $attachments);

          // $variables = array(
          //   'entry' => $cart
          // );
          // $html = \Craft::$app->view->renderTemplate('modules/market/order', $variables);

          $html = "Vielen Dank. <br><br> Wir bearbeiten Ihre Bestellung so bald wie möglich. <br><br> Sie können Ihre Bestellung unter folgendem Link einsehen: <br><br> https://www.grappa-bruno.ch/market/order/" . $cart->id . "/" . $cart->orderSecret;
          $subject = 'grappe-bruno.ch - Bestellung';
          $mail = 'hallo@grappa-bruno.ch';
          $attachments = array();
          Market::$plugin->MarketService->sendMail($html, $subject, $mail, $attachments);

          return $this->redirect('danke');
        }
  
      } else {
        // No valid order, not good. Redirect
        $this->redirect('market/checkout/confirmation');
      }

    }


    public function actionRemoveItemFromCart()
    {

      $this->requirePostRequest();
      $request = Craft::$app->getRequest();

      $itemId = $request->getParam('itemId', '');
  
      $cart = $this->_getCart(false);

      $success = Market::$plugin->MarketService->removeItemFromCart($itemId);

      $cartPrice = 0;
      $cartQuantity = 0;
      foreach ($cart->orderItems as $item) {
        $cartPrice = $cartPrice + $item->price * $item->quantity;
        $cartQuantity = $cartQuantity + $item->quantity;
      }

      if ($success) {
        $response = array(
          'success' => true,
          'message' => $itemId,
          'cartQuantity' => $cartQuantity,
          'cartPrice' => $cartPrice,
          'action' => 'success'
        );
      } else {
        $response = array(
          'success' => false,
          'message' => 'Could not remove item from cart.',
          'action' => 'error'
        );
      }

      if ($request->getAcceptsJson()) {
        return $this->asJson($response);
      }
    }

    /**
     * Fetches or creates an Entry.
     *
     * @return Poll
     * @throws NotFoundHttpException if the requested entry cannot be found
     */
    private function _getCart(bool $createCart = true)
    {

        return Market::$plugin->MarketService->getCart($createCart);

    }

}
