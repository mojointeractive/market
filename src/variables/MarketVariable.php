<?php
/**
 * Market plugin for Craft CMS 3.x
 *
 * mojo Market a simple commerce plugin for Craft 3
 *
 * @link      https://www.mojointeractive.ch/
 * @copyright Copyright (c) 2018 mojo interactive GmbH
 */

namespace mojointeractive\market\variables;

use mojointeractive\market\Market;
use craft\elements\Entry;

use Craft;

/**
 * Market Variable
 *
 * Craft allows plugins to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.market }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    mojo interactive GmbH
 * @package   Market
 * @since     1.0.0
 */
class MarketVariable
{
    // Public Methods
    // =========================================================================

    /**
     * Whatever you want to output to a Twig template can go into a Variable method.
     * You can have as many variable functions as you want.  From any Twig template,
     * call it like this:
     *
     *     {{ craft.market.exampleVariable }}
     *
     * Or, if your variable requires parameters from Twig:
     *
     *     {{ craft.market.exampleVariable(twigValue) }}
     *
     * @param null $optional
     * @return string
     */
    public function getCart($optional = null)
    {

        $cart = Entry::find()
        ->section('orders')
        ->orderSessionId(Craft::$app->getSession()->id)
        ->orderStatus('cart')
        ->order('postDate desc')
        ->one();

        if ($cart) {
          return $cart;
        } else {
          return null;
        }

    }

    public function getSessionID($optional = null)
    {

        return Craft::$app->getSession()->id;

    }
}
