<?php
/**
 * Market plugin for Craft CMS 3.x
 *
 * mojo Market a simple commerce plugin for Craft 3
 *
 * @link      https://www.mojointeractive.ch/
 * @copyright Copyright (c) 2018 mojo interactive GmbH
 */

namespace mojointeractive\market\services;

use mojointeractive\market\Market;

use Craft;
use craft\base\Component;
use craft\elements\Entry;
use craft\elements\MatrixBlock;
use craft\records\Section;
use craft\records\EntryType;
use craft\mail\Message;

/**
 * MarketService Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    mojo interactive GmbH
 * @package   Market
 * @since     1.0.0
 */
class MarketService extends Component
{
    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     Market::$plugin->marketService->exampleService()
     *
     * @return mixed
     */
    public function getCart($createCart = false)
    {

      $cart = Entry::find()
      ->section('orders')
      ->orderSessionId(Craft::$app->getSession()->id)
      ->orderStatus('cart')
      ->order('postDate desc')
      ->one();

      if ($cart) {
        return $cart;
      } else {

        if ($createCart == true) {

          $entryType = EntryType::find()->where(['handle' => 'order'])->one();

          // insert a new row of data
          $cart = new Entry();
          $cart->sectionId = $entryType->getAttribute('sectionId'); 
          $cart->typeId = $entryType->getAttribute('id');
          $cart->title = 'Order #';
          $cart->authorId = 1;
          $cart->orderStatus = 'cart';
          $cart->orderSessionId = Craft::$app->getSession()->id;

          if (!Craft::$app->getElements()->saveElement($cart)) {
            // if ($request->getAcceptsJson()) {
                return null;

                Craft::$app->getSession()->setError(Craft::t('market', 'Couldn’t save Cart.'));
            // }
          } else {
            return $cart;
          }


        } else {
          return null;
        }
      }

    }

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     Market::$plugin->marketService->exampleService()
     *
     * @return mixed
     */
    public function getProductById(int $productId = null): Entry
    {

      $product = Entry::find()
      ->section('products')
      ->id($productId)
      ->one();

      if ($product) {
        return $product;
      } else {
        return null;
      }

    }

    /**
     * Fetches or creates an Entry.
     *
     * @return Poll
     * @throws NotFoundHttpException if the requested entry cannot be found
     */
    public function checkItemInCart($cart, $productId, $variation, $options) {

      // Get the sku
      $items = $cart->orderItems;
      $response = false;
  
      foreach ($items as $item) {
        $variationMatch = false;
        if ($variation != false and $variation->id == $item->variationId) {
          $variationMatch = true;
        } elseif ($variation == false) {
          $variationMatch = true;
        }

        if($productId == $item->product->one()->id and $variationMatch) {
          $response = $item->id;
        } else {
          $response = false;
        }
      }
  
      return $response;
  
    }


    public function addItemToCart($cart, $productId, $variation, $options, $quantity) {

      $field = Craft::$app->getFields()->getFieldByHandle('orderItems');

      $existingMatrixQuery = $cart->getFieldValue('orderItems');
      $serializedMatrix = $field->serializeValue($existingMatrixQuery, $cart);

      // append the new blocks to the existing array
      $serializedMatrix['new1'] = [
        'type' => 'item',
        'fields' => [
          'product' => [$productId],
          'variationId' => $variation->id,
          'variationHeading' => $variation->heading,
          'quantity' => $quantity,
          'price' => $variation->price

        ]
      ];

      $cart->setFieldValue('orderItems', $serializedMatrix);

      // Save the Entry
      $success = Craft::$app->elements->saveElement($cart);
      if (!$success)
      {
          Craft::log('Couldn’t save the entry', LogLevel::Error);
          return $block->getErrors();
      }
      else {
        //   $cart->setContent(array(
        //     'marketOrderUpdated'										=> date("Y-m-d H:i:s")
        //   ));
  
        // // Save the updated Shipping infos
        // $updateDate = craft()->entries->saveEntry($cart);
      }
  
    }

    public function updateQuantity($cart, $itemId, $quantity) {

      $block = \craft\elements\MatrixBlock::find()
        ->id($itemId)
        ->one();

      $block->setFieldValue('quantity', $block->quantity + $quantity);



      // Save the Entry
      $success = Craft::$app->elements->saveElement($block);
      if (!$success)
      {
          Craft::log('Couldn’t save the entry', LogLevel::Error);
          return $block->getErrors();
      }
      else {
        //   $cart->setContent(array(
        //     'marketOrderUpdated'										=> date("Y-m-d H:i:s")
        //   ));
  
        // // Save the updated Shipping infos
        // $updateDate = craft()->entries->saveEntry($cart);
      }
  
    }

    public function removeItemFromCart($itemId) {

      Craft::$app->getElements()->deleteElementById($itemId);
        
        return true;
    }

    /**
 * @param $html
 * @param $subject
 * @param null $mail
 * @param array $attachments
 * @return bool
 */
public function sendMail($html, $subject, $mail = null, array $attachments = array()): bool
{
    $settings = Craft::$app->systemSettings->getSettings('email');
    $message = new Message();

    $message->setFrom([$settings['fromEmail'] => $settings['fromName']]);
    $message->setTo($mail);
    $message->setSubject($subject);
    $message->setHtmlBody($html);
    if (!empty($attachments) && \is_array($attachments)) {

        foreach ($attachments as $fileId) {
            if ($file = Craft::$app->assets->getAssetById((int)$fileId)) {
                $message->attach($this->getFolderPath() . '/' . $file->filename, array(
                    'fileName' => $file->title . '.' . $file->getExtension()
                ));
            }
        }
    }

    return Craft::$app->mailer->send($message);
}
}
