# Market Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## 1.0.0 - 2018-02-23
### Added
- Initial release

## 1.0.3 - 2018-08-19
### Added
- Misc Updates

## 1.0.4 - 2018-08-19
### Added
- Send marchent mail
